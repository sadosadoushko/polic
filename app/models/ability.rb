# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
  user ||= User.new # guest user (not logged in)
    if user.admin?
      can :manage, :all
    elsif user.moderator?
      can :read, :all
      can :manage, Post
    else
      can :read, :all
    end
  end
end
