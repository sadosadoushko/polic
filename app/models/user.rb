class User < ApplicationRecord
  enum role: [:user, :moderator, :admin]
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
end
